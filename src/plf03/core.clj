(ns plf03.core)

(defn función-comp-1
  []
  (let [f (fn [x] (inc x))
        g (fn [y] (- 5 y))
        z (comp f g)]
    (z 7)))

(defn función-comp-2
  []
  (let [f (fn [x] (+ 8 x))
        g (fn [y] (- 2 y))
        z(comp f g)]
    (z 1)))

(defn función-comp-3
  []
  (let [f (fn [xs] (filter pos? xs))
        g (fn [xs] (filter even? xs))
        h (fn [xs] (map inc xs))
        z (comp f g h)]
    (z #{-20 10 -3 5 -4 8 4 7 -10 12})))

(defn función-comp-4
  []
  (let [f (fn [xs] (into [] (remove pos? xs)))
        g (fn [xs] (reverse xs))
        h (fn [xs] (map inc xs))
        z (comp f g h)]
    (z [1 2 -5 4 -9 -6 -8])))

(defn función-comp-5
  []
  (let [f (fn [xs] (list? xs))
        g (fn [xs] (map-entry? xs))
        z (comp f g )]
    (z '(2 3 4 5))))

(defn función-comp-6
  []
  (let [f (fn [xs] (keyword xs))
        g (fn [xs] (str xs))
        z (comp f g)]
    (z [1 2]))) 

(defn función-comp-7
  []
  (let [f (fn [xs] (second xs))
        g (fn [xs] (reverse xs))
        z (comp f g)]
    (z '("a" 2 7 "b")))) 

(defn función-comp-8
  []
  (let [f (fn [x] (not x))
        g (fn [y] (zero? y))
        z (comp f g)]
    (z 10)))

(defn función-comp-9
  []
  (let [f (fn [x] (not x)) 
        g (fn [y] (zero? y))
        z (comp f g)]
    (z 5)))

(defn función-comp-10
  []
  (let [f (fn [x] (str x))
        g (fn [y] (double y))
        z (comp f g)]
    (z (+ 3 3 3))))

(defn función-comp-11
  []
  (let [f (fn [x] (char? x))
        g (fn [y] (first y))
        z (comp f g)]
    (z "hola")))

(defn función-comp-12
  []
  (let [f (fn [xs] (ident? xs))
        g (fn [xs] (empty xs))
        z (comp f g)]
    (z [1 2 3])))

(defn función-comp-13
  []
  (let [f (fn [xs] (indexed? xs))
        g (fn [xs] (vector xs))
        z (comp f g)]
    (z {:a 1 :b 2})))

(defn función-comp-14
  []
  (let [f (fn [xs] (integer? xs))
        g (fn [xs] (first xs))
        z (comp f g)]
    (z [\1 \b])))

(defn función-comp-15
  [] 
  ( let [ f (fn [x] (* 3 x))
          g (fn [y] (+ 2 y))
          z (comp f g)]
    (z 10)))  

(defn función-comp-16
  []
  (let [f (fn [x] (- 5 x))
        g (fn [y] (* 4 y))
        z (comp f g)]
    (z 2)))

(defn función-comp-17
  []
  (let [f (fn [x] (+ 12 x))
        g (fn [y] (+ 34 y))
        z (comp f g)]
    (z 10)))

(defn función-comp-18
  []
  (let [f (fn [x] (- 10 x))
        g (fn [y] (- 3 y))
        z (comp f g)]
    (z 25)))

(defn función-comp-19
  []
  (let [f (fn [x] (- x 50))
        g (fn [y] (* 3 y))
        z (comp f g)]
    (z 100)))

(defn función-comp-20
  []
  (let [f (fn [x] (* x 2))
        g (fn [y] (* y 3))
        z (comp f g)]
    (z 10)))



(función-comp-1) 
(función-comp-2)
(función-comp-3)
(función-comp-4)
(función-comp-5)
(función-comp-6) 
(función-comp-7)  
(función-comp-8)  
(función-comp-9) 
(función-comp-10) 
(función-comp-11) 
(función-comp-12) 
(función-comp-13) 
(función-comp-14)
(función-comp-15) 
(función-comp-16) 
(función-comp-17) 
(función-comp-18) 
(función-comp-19) 
(función-comp-20)

(defn función-complement-1
  []
  (let [f (fn [xs] (map even? xs))
        z(complement f)]
    (z '(1 2 3 4))))

(defn función-complement-2
  []
  (let [f (fn [x] (float? x))
        z (complement f)]
    (z 1)))

(defn función-complement-3
  []
  (let [f (fn [x] (associative? x))
        z (complement f)]
    (z "hola")))

(defn función-complement-4
  []
  (let [f (fn [x] (boolean? x))
        z (complement f)]
    (z (new Boolean "true"))))

(defn función-complement-5
  []
  (let [f (fn [xs] (coll? xs))
        z (complement f)]
    (z '("plf" "hola" "hello bro!"))))

(defn función-complement-6
  []
  (let [f (fn [xs] (map-entry? xs))
        z (complement f)]
    (z (last [1 2 2]))))

(defn función-complement-7
  []
  (let [f (fn [x] (map? x))
        z (complement f)]
    (z (inc 10))))

(defn función-complement-8
  []
  (let [f (fn [x] (nat-int? x))
        z (complement f)]
    (z (+ 10 1))))

(defn función-complement-9
  []
  (let [f (fn [x] (number? x))
        z (complement f)]
    (z 100)))

(defn función-complement-10
  []
  (let [f (fn [xs] (number? xs))
        z (complement f)]
    (z (last [1 2 3]))))

(defn función-complement-11
  []
  (let [f (fn [x] (pos-int? x))
        z (complement f)]
    (z 10M)))

(defn función-complement-12
  []
  (let [f (fn [x] (rational? x))
        z (complement f)]
    (z (+ 2 1/4 3/8))))

(defn función-complement-13
  []
  (let [f (fn [x] (boolean? x))
        z(complement f)]
    (z true)))

(defn función-complement-14
  []
  (let [f (fn [x] (float? x))
        z (complement f)]
    (z 40)))

(defn función-complement-15
  []
  (let [f (fn [x] (pos? x))
        z (complement f)]
    (z 1)))

(defn función-complement-16
  []
  (let [f (fn [x] (odd? x))
        z (complement f)]
    (z 154)))

(defn función-complement-17
  []
  (let [f (fn [x] (odd? x))
        z (complement f)]
    (z 22)))

(defn función-complement-18
  []
  (let [f (fn [xs] (associative? xs))
        z (complement f)]
    (z [1 3 4])))

(defn función-complement-19
  []
  (let [f (fn [x] (char? x))
        z (complement f)]
    (z :b)))

(defn función-complement-20
  []
  (let [f (fn [x] (keyword? x))
        z (complement f)]
    (z :hola!)))

(función-complement-1)
(función-complement-2)
(función-complement-4)
(función-complement-3)
(función-complement-5)
(función-complement-6)
(función-complement-7)
(función-complement-8)
(función-complement-9)
(función-complement-10)
(función-complement-11)
(función-complement-12)
(función-complement-13)
(función-complement-14)
(función-complement-15)
(función-complement-16)
(función-complement-17)
(función-complement-19)
(función-complement-18)
(función-complement-20)

(defn función-constantly-1 
  []
  (let [f '(1 2 3)
        z (constantly f)]
    (z 100)))

(defn función-constantly-2
  []
  (let [f "megamente"
        z (constantly f)]
    (z '(10 1 2))))

(defn función-constantly-3
  []
  (let [f [1 2 3]
        z (constantly f)]
    (z  {1 2 3 4})))

(defn función-constantly-4
  []
  (let [f "plf"
        z (constantly f)]
    (z {:a :b})))

(defn función-constantly-5
  []
  (let [f \1
        z (constantly f)]
    (z {:a 1})))

(defn función-constantly-6
  []
  (let [f [:a "hola " :b "   "]
        z (constantly f)]
    (z  #{1 2 3 4})))

(defn función-constantly-7
  []
  (let [f (inc 10)
        z (constantly f)]
    (z  #{1 2 3 4})))

(defn función-constantly-8
  []
  (let [f (dec 200)
        z (constantly f)]
    (z  #{"87" "hola"}))) 

(defn función-constantly-9
  []
  (let [f (char? \D)
        z (constantly f)]
    (z (iterate inc 20))))

(defn función-constantly-10
  []
  (let [f (first [1 2 3 4])
        z (constantly f)]
    (z 2)))

(defn función-constantly-11
  []
  (let [f 115.0
        z (constantly f)]
    (z  char? 20)))

(defn función-constantly-12
  []
  (let [f []
        z (constantly f)]
    (z double? 40))) 

(defn función-constantly-13
  []
  (let [f "bernat"
        z (constantly f)]
    (z double? 4))) 

(defn función-constantly-14
  []
  (let [f [:a 1 :b 2 :c 3 :d 4]
       z (constantly f)]
    (z true 10)))

(defn función-constantly-15
  []
  (let [f  9999
        z (constantly f)]
    (z  int? 200)))

(defn función-constantly-16
  []
  (let [f  "calvin harris"
        z (constantly f)]
    (z  rational? ["hola" "plf" "g"])))

(defn función-constantly-17
  []
  (let [f  false
        z (constantly f)]
    (z  '([1234 4 1]))))

(defn función-constantly-18
  []
  (let [x  {true true false false} 
        f (constantly x)]
    (f  "hello moto")))

(defn función-constantly-19
  []
  (let [x  ["hola " \a 1 :a]
        f (constantly x)]
    ( f boolean?  true )))

(defn función-constantly-20
  []
  (let [x  [1 2 3 4 5]
        f (constantly x)]
    (f (+ 2 4))))



(función-constantly-1)
(función-constantly-2)
(función-constantly-3)
(función-constantly-4)
(función-constantly-5)
(función-constantly-6)
(función-constantly-7)
(función-constantly-8)
(función-constantly-9)
(función-constantly-10)
(función-constantly-11)
(función-constantly-12)
(función-constantly-13)
(función-constantly-14)
(función-constantly-15)
(función-constantly-16)
(función-constantly-17)
(función-constantly-18)
(función-constantly-19)
(función-constantly-20)

(defn función-every-pred-1
  []
  (let [f (fn [xs] (number? xs))
        g (fn [xs] (- 5 xs)) 
        z (every-pred f g)]
    (z  [1 2 3 4] )))

(defn función-every-pred-2
  []
  (let [f (fn [xs] (number? xs))
        g (fn [xs] (+ 15 xs)) 
        z (every-pred f g)]
    (z  [1 2 3 4] ))) 

(defn función-every-pred-3
  []
  (let [f (fn [xs] (number? xs))
        g (fn [xs] (first xs)) 
        z (every-pred f g)]
    (z [1 2 3 4]))) 

(defn función-every-pred-4
  []
  (let [f (fn [x] (int? x))
        g (fn [y] (filter even? y))
        z (every-pred f g)]
    (z :a :c)))

(defn función-every-pred-5
  []
  (let [f (fn [x] (filter pos? x))
        g (fn [y] (filter number? y))
        z (every-pred f g)]
    (z 3 -3/4)))

(defn función-every-pred-6
  []
  (let [f (fn [xs] (filter int? xs))
        g (fn [xs] (filter dec xs))
        z (every-pred f g)]
    (z '(2 1 4 \a))))

(defn función-every-pred-7
   []
   (let [f (fn [x] (even? x))
         g (fn [y] (pos? y))
         z (every-pred f g)] 
     (z 2))) 

 (defn función-every-pred-8
   []
   (let [f (fn [x] (even? x))
         g (fn [y] (neg? y))
         z (every-pred f g)]
     (z -2))) 
 
 (defn función-every-pred-9
   []
   (let [f (fn [x] (pos? x))
         g (fn [y] (double? y))
         z (every-pred f g)]
     (z 20.10)))
 
 (defn función-every-pred-10
   []
   (let [f (fn [x] (neg? x))
         g (fn [y] (double? y))
         z (every-pred f g)]
     (z 20.10)))

(defn función-every-pred-11
  []
  (let [f (fn [x] (filter vector x))
        g (fn [y] (filter number? y))
        z (every-pred f g)]
    (z 3 -3/4)))

(defn función-every-pred-12
  []
  (let [f (fn [xs] (filter float? xs))
        g (fn [xs] (filter number? xs))
        z (every-pred f g)]
    (z #{"a" "b" :c}))) 

(defn función-every-pred-13
  []
  (let [f (fn [xs] (indexed? xs))
        g (fn [xs] (list xs))
        z (every-pred f g)]
    (z {:a 1 :b 2})))

(defn función-every-pred-14
  []
  (let [f (fn [x] (+ 2 x))
        g (fn [y] (+ 1 y))
        z (every-pred f g)]
    (z 10))) 

(defn función-every-pred-15
  []
  (let [f (fn [x] (* x 1.0))
        g (fn [y] (/ y 10))
        z (every-pred f g)]
    (z 1))) 

(defn función-every-pred-16
  []
  (let [f (fn [xs] (filter pos? xs))
        g (fn [xs] (filter even? xs))
        z (every-pred f g)]
    (z #{-1 1 -2 2 3 4 -45 0})))
 
(defn función-every-pred-17
  []
  (let [f (fn [xs] (filter first xs))
        g (fn [xs] (filter number? xs))
        z (every-pred f g)]
    (z #{1 23 4 5 :a :C}))) 

(defn función-every-pred-18
  []
  (let [f (fn [x] (boolean? x))
        g (fn [y] (filter vector y))
        z (every-pred f g)]
    (z (new Boolean "false"))))

(defn función-every-pred-19
  []
  (let [f (fn [x] (float? x))
        g (fn [y] (filter list y))
        z (every-pred f g)]
    (z (+ 1 1/2 2.5))))

(defn función-every-pred-20
  []
  (let [f (fn [xs] (map-entry? xs))
        g (fn [xs] (filter xs))
        z (every-pred f g)]
    (z (first [1 2 3]))))

(función-every-pred-1)
(función-every-pred-2)
(función-every-pred-3)
(función-every-pred-4)
(función-every-pred-5)
(función-every-pred-6)
(función-every-pred-7)
(función-every-pred-8)
(función-every-pred-9)
(función-every-pred-10)
(función-every-pred-11)
(función-every-pred-12)
(función-every-pred-13)
(función-every-pred-14)
(función-every-pred-15)
(función-every-pred-16)
(función-every-pred-17)
(función-every-pred-18)
(función-every-pred-19)
(función-every-pred-20)

(defn función-fnil-1
  []
  (let [f (fn [x] (inc x))
        g (fn [xs] (* 10 xs))
        z (fnil f g)]
    (z 10)))

(defn función-fnil-2
  []
  (let [f (fn [x] (inc x)) 
        g (fn [y] (dec y))
        z (fnil f g)]
    (z 8)))

(defn función-fnil-3
  []
  (let [f (fn [x] (str "calvin " x))
        z (fnil f " ")]
    (z "harris")))

(defn función-fnil-4
  []
  (let [f (fn  [x y] (str "ey " x " de " y))
        z (fnil f "")]
    (z "tamo" "vuelta")))  

(defn función-fnil-5
  []
  (let [f (fn  [x] (str "isr" x))
        z (fnil f "")]
    (z "ael"))) 

(defn función-fnil-6
  []
  (let [f (fn [x] (inc x))
        z (fnil f nil)]
    (z 10)))  

(defn función-fnil-7
  []
  (let [f (fn [x] (nil? x))
        z (fnil f (nil? true))]
    (z true))) 

(defn función-fnil-8
  []
  (let [f (fn  [x] (str "hola "  x))
        z (fnil f "")]
    (z "my friend"))) 

(defn función-fnil-9
  []
  (let [f (fn  [x] (/ 10 x))
        z (fnil f nil)]
    (z 10)))

(defn función-fnil-10
  []
  (let [f (fn  [x] (+ 1 x))
        z (fnil f nil)]
    (z 1)))

(defn función-fnil-11
  []
  (let [f (fn  [x] (- 52 x))
        z (fnil f nil)]
    (z 6)))

(defn función-fnil-12
  []
  (let [f (fn  [x] (+ 2 x))
        z (fnil f nil)]
    (z 14)))

(defn función-fnil-13
  []
  (let [f (fn [x] [:a x] [:b])
        z (fnil f inc)]
    (z 2)))

(defn función-fnil-14
  []
  (let [f (fn [x] [:a x] [:b])
        z (fnil f nil)]
    (z 10.0))) 

(defn función-fnil-15
  []
  (let [f (fn  [x] (str "Logica " x ))
        z (fnil f "")]
    (z 10)))

(defn función-fnil-16
  []
  (let [f (fn [x] (x))
        z (fnil inc f)]
    (z 10))) 

(defn función-fnil-17
  []
  (let [f (fn [x] (decimal? x))
        g (fn [y] (* 10 y))
        z (fnil f g)] 
    (z 1)))

(defn función-fnil-18
  []
  (let [f (fn [xs] (first xs))
        g (fn [xs] (* 10 xs))
        z (fnil f g)]
    (z [1 2 3])))

(defn función-fnil-19
  []
  (let [f (fn [xs] (last xs))
        g (fn [xs] (* 10 xs))
        z (fnil f g)]
    (z '(1 10 5))))

(defn función-fnil-20
  []
  (let [f (fn [xs] (map-entry? xs))
        g (fn [xs] (* 10 xs))
        z (fnil f g)]
    (z '(1 10 5))))

(función-fnil-1)
(función-fnil-2)
(función-fnil-3)
(función-fnil-4)
(función-fnil-5)
(función-fnil-6)
(función-fnil-7)
(función-fnil-8)
(función-fnil-9)
(función-fnil-10)
(función-fnil-11)
(función-fnil-12)
(función-fnil-13)
(función-fnil-14)
(función-fnil-15)
(función-fnil-16)
(función-fnil-17)
(función-fnil-18)
(función-fnil-19)
(función-fnil-20)

(defn función-juxt-1
  []
  (let [f (fn [x] (first x))
        g (fn [y] (count y))
        z (juxt f g)]
    (z "hola")))

(defn función-juxt-2
  []
  (let [f (fn [xs] (take 3 xs))
        g (fn [xs] (drop 4 xs))
        z (juxt f g)]
    (z [1 2 3 4])))

(defn función-juxt-3
  []
  (let [f (fn [xs] (last xs))
        g (fn [xs] (str xs))
        z (juxt g f)]
    (z [1 2 3 4])))

(defn función-juxt-4
  []
  (let [f (fn [xs] (map inc xs))
        g (fn [xs] (filter odd? xs))
        z (juxt g f)]
    (z [1 2 3 4 5])))

(defn función-juxt-5
  []
  (let [f (fn [xs] (count xs))
        g (fn [xs] (first xs))
        z (juxt g f)]
    (z ["a" 3 3 3 4 5]))) 

(defn función-juxt-6
  []
  (let [f (fn [x] (+ 52 x))
        g (fn [y] (+ 0 y))
        z (juxt f g)]
    (z 1)))

(defn función-juxt-7
  []
  (let [f (fn [xs] (filter true? xs))
        g (fn [xs] (count xs))
        z (juxt f g)] 
    (z '(1 \N "h" \1 2 "harris" ))))

(defn función-juxt-8
  []
  (let [f (fn [xs] (last xs))
        g (fn [xs] (list xs))
        z (juxt f g)]
    (z [ 15 33 100 5])))

(defn función-juxt-9
  []
  (let [f (fn [x] (take 5 x))
        g (fn [y] (reverse y))
        z (juxt g f)]
    (z "aloh")))


(defn función-juxt-10
  []
  (let [f (fn [x] (range x))
        g (fn [y] (+ 4 y))
        z (juxt f g)]
    (z 1)))

(defn función-juxt-11
  []
  (let [f (fn [xs] (map key xs))
        g (fn [xs] (map val xs))
        z (juxt f g)]
    (z {:a 1 :b 2 :c 3 :d 4 })))

(defn función-juxt-12
  [] 
  (let [f (fn [xs] (count xs))
        g (fn [xs] (:a xs))
        z (juxt f g)]
    (z {:a 1 :b 2 :c 2})))

(defn función-juxt-13
  []
  (let [f (fn [x] (sort x))
        g (fn [y] (conj [100 200] y))
        z (juxt f g)]
    (z "plf")))

(defn función-juxt-14
  []
  (let [f (fn [x] (sort x))
        g (fn [y] (conj [1] y))
        z (juxt f g)]
    (z "gool")))

(defn función-juxt-15
  []
  (let [f (fn [xs] (remove true? xs))
        g (fn [xs] (last xs))
        z (juxt f g)]
    (z '(1 true -1 false 7 false 0 true))))

(defn función-juxt-16
  []
  (let [f (fn [xs] (vector? xs))
        g (fn [xs] (list? xs))
        h (fn [xs] (map? xs))
        z (juxt g f h)]
    (z '("hola" "diego" "hola" "latauro"))))

(defn función-juxt-17
  []
  (let [f (fn [xs] (take-last 2 xs))
        g (fn [xs] (count xs))
        z (juxt f g)]
    (z [1 2 3 4])))

(defn función-juxt-18
  []
  (let [f (fn [xs] (drop-last xs))
        g (fn [xs] (remove pos? xs))
        z (juxt g f)]
    (z #{11 0 -2 4 5 -1 -3})))

(defn función-juxt-19
  []
  (let [f (fn [xs] (sort-by count xs))
        g (fn [xs] (string? xs))
        z (juxt f g)]
    (z ["a" "a" "b" "c"])))

(defn función-juxt-20
  []
  (let [f (fn [x] (last x))
        g (fn [y] (count y))
        z (juxt f g)]
    (z "f2k")))
 
(función-juxt-1)
(función-juxt-2)
(función-juxt-3)
(función-juxt-4)
(función-juxt-5)
(función-juxt-6)
(función-juxt-7)
(función-juxt-8)
(función-juxt-9)
(función-juxt-10)
(función-juxt-11)
(función-juxt-12)
(función-juxt-13)
(función-juxt-14)
(función-juxt-15)
(función-juxt-16)
(función-juxt-17)
(función-juxt-18)
(función-juxt-19)
(función-juxt-20)

(defn función-partial-1
  []
   (let [f (fn [x] (+ 10 x))
        z (partial f)]
    (z 1)))

(defn función-partial-2
  []
  (let [f (fn [x] (/ 1 x))
        z (partial f)]
    (z 1)))

(defn función-partial-3
  []
  (let [f (fn [x] (- 100 x))
        z (partial f)]
    (z 50)))

(defn función-partial-4
  []
  (let [f (fn [x] (/ 10 x))
        z (partial f)]
    (z 12)))

(defn función-partial-5
  []
  (let [f (fn [x] (/ x ))
        z (partial f)]
    (z 1)))

(defn función-partial-6
  []
  (let [f (fn [x ] (vector x))
        z (partial f)]
    (z (+ 1 2 3))))

(defn función-partial-7
  []
  (let [f (fn [x] (list x))
        z (partial f)]
    (z (* 1 2 3 4))))

(defn función-partial-8
  []
  (let [f (fn [xs] (list xs))
        z (partial f)]
    (z [10 20 30 40])))

(defn función-partial-9
  []
  (let [f (fn [x] (filter char? x))
        z (partial f)]
    (z "goool")))

(defn función-partial-10
  []
  (let [f (fn [x] (group-by char? x))
        z (partial f)]
    (z "rasemgan")))

(defn función-partial-11
  []
  (let [f (fn [x] (sort-by char? x))
        z (partial f)]
    (z "izanami")))

(defn función-partial-12
  []
  (let [f (fn [x] (take-while char? x))
        z (partial f)]
    (z "chidori")))

(defn función-partial-13
  []
  (let [f (fn [x] (sequential? x))
        z (partial f)]
    (z "plf")))

(defn función-partial-14
  []
  (let [f (fn [x] (remove char? x))
        z (partial f)]
    (z "saint seiya")))

(defn función-partial-15
  []
  (let [f (fn [xs] (filterv even? xs))
        z (partial f)]
    (z [1 2 3 4])))

(defn función-partial-16
  []
  (let [f (fn [xs] (filter pos? xs))
        z (partial f)]
    (z [-1 20 -3 4])))

(defn función-partial-17
  []
  (let [f (fn [xs] (filter pos? xs))
        z (partial f)]
    (z [1 2 3 4])))

(defn función-partial-18
  []
  (let [f (fn [xs] (take-while string? xs))
        z (partial f)]
    (z '("hola" :a 1 :b 12))))

(defn función-partial-19
  []
  (let [f (fn [xs] (take-while number? xs))
        z (partial f)]
    (z [true true false true])))

(defn función-partial-20
  []
  (let [f (fn [xs] (take-while boolean? xs))
        z (partial f)]
    (z [false true -1 5000M] )))

(función-partial-1)
(función-partial-2)
(función-partial-3)
(función-partial-4)
(función-partial-5)
(función-partial-6)
(función-partial-7)
(función-partial-8)
(función-partial-9)
(función-partial-10)
(función-partial-11)
(función-partial-12)
(función-partial-13)
(función-partial-14)
(función-partial-15)
(función-partial-16)
(función-partial-17)
(función-partial-18)
(función-partial-19)
(función-partial-20)

(defn función-some-fn-1 
  [] 
  (let [ f (fn [x] (pos? x))
         g (fn [y] (even? y))
         z (some-fn f g)]
    (z 1)))

(defn función-some-fn-2
  []
  (let [f (fn [x] (pos? x))
        g (fn [y] (even? y))
        z (some-fn f g )]
    (z 10)))

(defn función-some-fn-3
  []
  (let [f (fn [x] (pos? x))
        g (fn [y] (even? y))
        z (some-fn f g)]
    (z 10)))

(defn función-some-fn-4
  []
  (let [f (fn [x] (pos? x))
        g (fn [y] (odd? y))
        z (some-fn f g)]
    (z 100)))

(defn función-some-fn-5
  []
  (let [f (fn [x] (pos? x))
        g (fn [y] (odd? y))
        h (fn [s] (float? s))
        z (some-fn f g h)]
    (z 5)))

(defn función-some-fn-6
  []
  (let [f (fn [x] (pos? x))
        g (fn [y] (odd? y))
        h (fn [s] (int? s))
        z (some-fn f g h)]
    (z 1.0)))

(defn función-some-fn-7
  []
  (let [f (fn [x] (neg? x))
        g (fn [y] (even? y))
        h (fn [s] (false? s))
        z (some-fn f g h)]
    (z 10)))

(defn función-some-fn-8
  []
  (let [f (fn [x] (neg? x))
        g (fn [y] (even? y))
        z (some-fn f g)]
    (z 14)))

(defn función-some-fn-9
  []
  (let [f (fn [x] (neg? x))
        g (fn [y] (even? y))
        z (some-fn f g)]
    (z 14)))

(defn función-some-fn-10
  []
  (let [f (fn [x] (neg? x))
        g (fn [y] (odd? y))
        z (some-fn f g)]
    (z 0)))

(defn función-some-fn-11
  []
  (let [f (fn [x] (neg? x))
        g (fn [y] (odd? y))
        z (some-fn f g)]
    (z 12)))

(defn función-some-fn-12
  []
  (let [f (fn [x] (neg? x))
        g (fn [y] (odd? y))
        z (some-fn f g)]
    (z -4)))

(defn función-some-fn-13
  []
  (let [f (fn [x] (pos? x))
        g (fn [y] (float? y))
        z (some-fn f g)]
    (z 23)))

(defn función-some-fn-14
  []
  (let [f (fn [x] (pos? x))
        g (fn [y] (int? y))
        z (some-fn f g)]
    (z 14)))

(defn función-some-fn-15
  []
  (let [f (fn [x] (neg? x))
        g (fn [y] (double? y))
        z (some-fn f g)]
    (z 23)))

(defn función-some-fn-16
  []
  (let [f (fn [x] (neg? x))
        g (fn [y] (int? y))
        z (some-fn f g)]
    (z 41)))

(defn función-some-fn-17
  []
  (let [f (fn [x] (neg? x))
        g (fn [y] (int? y))
        z (some-fn f g)]
    (z 12)))

(defn función-some-fn-18
  []
  (let [f (fn [x] (pos? x))
        g (fn [y] (double? y))
        z (some-fn f g)]
    (z -52)))

(defn función-some-fn-19
  []
  (let [f (fn [x] (even? x))
        g (fn [y] (int? y))
        z (some-fn f g)]
    (z 12)))

(defn función-some-fn-20
  []
  (let [f (fn [x] (odd? x))
        g (fn [y] (int? y))
        z (some-fn f g)]
    (z 12)))


(función-some-fn-1)
(función-some-fn-2)
(función-some-fn-3)
(función-some-fn-4)
(función-some-fn-5)
(función-some-fn-6)
(función-some-fn-7)
(función-some-fn-8)
(función-some-fn-9)
(función-some-fn-10)
(función-some-fn-11)
(función-some-fn-12)
(función-some-fn-13)
(función-some-fn-14)
(función-some-fn-15)
(función-some-fn-16)
(función-some-fn-17)
(función-some-fn-18)
(función-some-fn-19)
(función-some-fn-20)










